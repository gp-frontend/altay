(function () {

	var ESC_KEYCODE = 27;
	var ENTER_KEYCODE = 13;

	/*----------------------------------------
	POPUPS / MODALS
	----------------------------------------*/
	var popup = $(".popup"),
			popupOpen = $("[data-popup='open']"),
			popupClose = $("[data-popup='close']"),
			popupOverlay = $(".popup-overlay");

	/* POPUP FUNCTIONS */
	$.fn.gpPopup = function( method ) {

		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.show.apply( this, arguments );
		} else {
			$.error( 'Метод с именем ' +  method + ' не существует' );
		}

	};

	//WIDTH SCROLL BAR
	var scrollBar = document.createElement('div');

	scrollBar.style.overflowY = 'scroll';
	scrollBar.style.width = '50px';
	scrollBar.style.height = '50px';

	// при display:none размеры нельзя узнать
	// нужно, чтобы элемент был видим,
	// visibility:hidden - можно, т.к. сохраняет геометрию
	scrollBar.style.visibility = 'hidden';

	document.body.appendChild(scrollBar);
	var scrollWidth = scrollBar.offsetWidth - scrollBar.clientWidth;
	document.body.removeChild(scrollBar);

	// alert( scrollWidth );

	/* POPUP METHODS */
	var methods = {
		//show popup
		show : function( options ) {
			return this.each(function(){
				popup.removeClass('popup_open');
				$(this).addClass('popup_open');
				$('body').addClass('locked');
				if(scrollWidth > 0){
					$('body').css('padding-right', scrollWidth);
				}
			});
		},
		//hide all popups
		hide : function( ) {
			return this.each(function(){
				if (popup.hasClass('popup_open')) {
					popup.removeClass('popup_open');
					$('body').removeClass('locked');
					$('body').css('padding-right', '');
				}
			})

		}
	};

	/* OPEN */
	function openPopup(e){
		e.preventDefault();
		var id = $(this).attr('data-popup-id');
		$(id).gpPopup();
	}

	/* CLOSE */
	function closePopup(e){
		e.preventDefault();
		popup.gpPopup('hide');
	}

	/* ADD CLICK FUNCTION */
	popupOpen.click(openPopup);
	popupClose.click(closePopup);
	popupOverlay.click(closePopup);

	/* PRESS ESC BUTTON */
	$(document).keydown(function(evt) {
		if( evt.keyCode === ESC_KEYCODE ) {
			popup.gpPopup('hide');
			return false;
		}
	});


})();
